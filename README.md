#Transmit text to a pastebin

 [stdin] | topastebin.rc [pastebin] file

 output will be url to pastebin generated


##Requires

 * Byron's rc shell
 * curl
 * xsel
 * unix core utilities (test, cat)

##Pastebins

 * sprunge
 * termbin
